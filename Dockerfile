FROM golang:latest

WORKDIR /app/web-server/
COPY ./web-client/ ./web-client/
COPY ./web-api/ ./web-api/

WORKDIR /app/web-server/web-client
RUN go get -d -v
RUN go build -v

WORKDIR /app/web-server/web-client
RUN go get -d -v
RUN go build -v

