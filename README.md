# Web Server

This is a small docker web server with mongoDB and golang.

## Features

- [ ] A working api
- [x] A main website
- [ ] Save data to mongoDB
- [ ] ...

## Description

This is a project to save feedback from a game and to have a website for the game (I haven't made the game yet lol). The idee is that in the game you press a button and than you get a feedback menu where you can type how you feel about te game at that moment. Then you press upload and the feedback is send to my server via the api. This api saves the feedback to the mongoDB. You can see all the feedback data via the web site (this is called web-client in my project).

The web server currently runs on docker compose, I might change this to kubernetes in the futur.

## Website

- [ ] TODO: Add screenshots here
- [ ] TODO: explain the website

## Installation

``` bash
git clone https://gitlab.com/livy_y/docker-web-server
cd docker-web-server

docker-compose up -d
```

## License

MIT-license
