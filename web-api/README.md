# Server API

## Example of feedback report
```json
{
    "feedback example": {
        "id": 1,                  # the report id
        "day name": 3,            # the day of the week (1=monday, 2=thursday, ...)
        "day number":  22,        # the day in the month
        "month":  12,             # the month
        "year":  2021,            # the year
        "hour":  16,              # the current hour
        "minutes": 8,             # the current minute
        "x coordinate":  54,      # the x coordinate in the game
        "y coordinate":  578,     # the y coordinate in the game
        "z coordinate": 284,      # the z coordinate in the game
        "happiness": 8,           # how happy you are on a scale from 1-10
        "note": "cool mountain",  # some extra info
        "image id": 0,            # the id of the screenshot (0 means no image)
        "tag": "report"           # the type of feedback (report, cool, ...)
    }
}

```