package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	listen = flag.String("listen", ":10121", "listen address")

	main_page = flag.String("main page", "./public/main", "the main website page")
)

func main() {
	flag.Parse()
	log.Printf("listening on %q...", *listen)

	http.Handle("/", http.FileServer(http.Dir(*main_page)))

	log.Fatal(http.ListenAndServe(*listen, nil))
}
